import React, { useEffect, useState } from 'react';
import { getStoryIds } from './services/hnApi';

export const App = () => {
  const [storyIds, setStoryIds] = useState([]);
  
  useEffect(() => {
    console.log(getStoryIds())
    getStoryIds().then(data => setStoryIds(data));
    return () => {};
  }, [])

  return (
    <p>{JSON.stringify(storyIds)}</p>
  )
}